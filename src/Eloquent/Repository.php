<?php

namespace Fero\Repositories\Eloquent;

use Fero\Repositories\Contracts\RepositoryInterface;
use Fero\Repositories\Exceptions\RepositoryException;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 * @package Fero\Repositories\Eloquent
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Repository constructor.
     * @param App $app
     *
     * @throws \Fero\Repositories\Exceptions\RepositoryException
     */
    function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract function model();

    /**
     * @return Model
     *
     * @throws \Fero\Repositories\Exceptions\RepositoryException
     */
    public function makeModel() {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new RepositoryException("Class {$this->model()} must by an instance of Illuminate\\Database\\Model");
        }

        return $this->model = $model;
    }
}