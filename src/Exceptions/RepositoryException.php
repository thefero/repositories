<?php

namespace Fero\Repositories\Exceptions;

/**
 * Class RepositoryException
 * @package Fero\Repositories\Exceptions
 */
class RepositoryException extends \Exception
{

}